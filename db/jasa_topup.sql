-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 16 Mar 2022 pada 03.15
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jasa_topup`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `no_cust` varchar(100) NOT NULL,
  `hrg_pokok` int(11) NOT NULL,
  `hrg_jual` int(11) NOT NULL,
  `laba` int(11) NOT NULL,
  `lunas_blmlns` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tanggal`, `jenis`, `no_cust`, `hrg_pokok`, `hrg_jual`, `laba`, `lunas_blmlns`) VALUES
(54, '2022-03-11', 'F tf 75k mm', '90897980808', 75000, 76000, 1000, 'Belum Lunas'),
(60, '2022-03-11', 'F BCA 70k', '787887879', 70000, 74000, 4000, 'Belum Lunas'),
(61, '2022-03-12', 'C Pulsa 10k', '895402955143', 50223, 51000, 777, 'Lunas'),
(62, '2022-03-13', 'C Token 20k', '523511899045', 20150, 21000, 850, 'Lunas'),
(63, '2022-03-13', 'F Listrik 20k', '1449813322', 20150, 21000, 850, 'Lunas'),
(64, '2022-03-13', 'C Shopee', '089389231321', 40000, 41000, 1000, 'Lunas'),
(65, '2022-03-13', 'C Dana 150k', '08937125837', 150000, 153000, 3000, 'Lunas');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
