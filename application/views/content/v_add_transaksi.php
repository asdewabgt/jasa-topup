<div class="content-wrapper">
    <div class="content-header">
    </div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-body">
                        <form id="form-tambah-transaksi" method="post" action="<?= site_url('Transaksi/proses_simpan') ?>" role="form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Tanggal</label>
                                        <input type="date" class="form-control form-control-sm" id="tanggal" name="tanggal" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Jenis Topup</label>
                                        <input type="text" class="form-control form-control-sm" id="jenis" name="jenis" required>
                                    </div>
                                    <div class="form-group">
                                        <label>No. Pelanggan</label>
                                        <input type="number" class="form-control form-control-sm" id="no_cust" name="no_cust" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Harga Pokok</label>
                                        <input type="number" class="form-control form-control-sm" id="hrg_pokok" name="hrg_pokok" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Harga Jual</label>
                                        <input type="number" class="form-control form-control-sm" id="hrg_jual" name="hrg_jual" required>
                                    </div>
                                    <div class="form-group">
										<label for="">Cicilan</label>
										<select name="lunas_blmlns" class="form-control" id="lunas_blmlns">
											<option value="Lunas">Lunas</option>
											<option value="Belum Lunas">Belum Lunas</option>
										</select>
									</div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <button id="btn-save-transaksi" type="button" class="btn btn-success"><i class="fas fa-file-export"></i>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function() {
        $("#btn-save-transaksi").on("click", function() {
            let validate = $("#form-tambah-transaksi");
            if (validate) {
                $("#form-tambah-transaksi").submit();
            }
        });
    });
</script>
