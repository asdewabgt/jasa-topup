<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div>

			</div>
		</div>
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info">
					<div class="card-body">
						<form id="form-ubah-transaksi" method="post" action="<?= site_url('Saldo/proses_update') ?>"
							  role="form">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>Nama</label>
										<input type="text" class="form-control form-control-sm" id="nama"
											   name="nama" value="<?= $saldos->nama ?>" placeholder="Enter ..."
											   required>
									</div>
									<div class="form-group">
										<label>Saldo</label>
										<input type="text" class="form-control form-control-sm" id="saldo"
											   name="saldo" value="<?= $saldos->saldo ?>" placeholder="Enter ..."
											   required>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<button id="btn-save" class="btn btn-sm btn-success"><i class="fas fa-edit"></i>Ubah
								</button>
							</div>
							<input type="hidden" id="id" name="id"
								   value="<?= $saldos->id_saldo; ?>"/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--  -->
</div>